﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("CSharp/Camera-Control/Fixed Follow")]

public class FixedFollow : MonoBehaviour
{
		private float aX;//mouse angle x
		private float aY;//mouse angle y
		private float aZ;//mouse angle z
		//public float mX;//mouse X axis
		public float distance = 5.0f;
		public float height = 10.0f;
		private Vector3 targetvec;
		private Vector3 camrotvec;
		private Vector3 camrailvec;
		private Vector3 camrail_eulervec;
		private Quaternion camrail_rotvec;
		private Transform transtarget;
		private bool isCamLocked;//always will be locked unless player presses unlock key. Unlocked allows camera to rotate around player.
		//public Transform transcamrail;
	
		void Start ()
		{
//				isCamLocked = true;
				camrail_eulervec = transform.eulerAngles;
				aX = camrail_eulervec.x;
				aY = camrail_eulervec.y;
				aZ = camrail_eulervec.z;
				transtarget = GameObject.FindGameObjectWithTag ("Player").transform;
		}

		void Update ()
		{
//				if (isCamLocked == true && Input.GetButton ("CamLock")) {
//						Debug.Log ("camlock off");
//						isCamLocked = false;//player can rotate camera with mouse now.
//				} else if (Input.GetButtonUp ("CamLock")) {
//						isCamLocked = true;
//				}
		}

		void LateUpdate ()
		{
//				if (isCamLocked == false) {
//						aX += Input.GetAxis ("Mouse X") * 250 * 0.02f;
//						aY += Input.GetAxis ("Mouse Y") * 250 * 0.02f;
//						camrail_rotvec = Quaternion.Euler (aX, aY, 0);
//				} else {
//						if (isCamLocked == true) {
//						
//						}
//				}
				camrailvec = transform.position;
				targetvec = transtarget.transform.position;
				camrotvec = transform.localEulerAngles;
	
				camrailvec.x = targetvec.x - distance;
				camrailvec.y = height;
				camrailvec.z = targetvec.z - distance;

				//reflect it back
				transform.position = camrailvec;
				transtarget.transform.position = targetvec;
				transform.localEulerAngles = camrotvec;

				transform.LookAt (targetvec);

		}
}