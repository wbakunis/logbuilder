﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("CSharp/Camera-Control/CamController")]

public class CamController : MonoBehaviour
{

		public float distance;
		public float[] camdistances;//list of all camera distances.
		public float cpos;
		public float cpossqr;
		public Camera cobj;//closest camera to player
		public Camera[] mapcam_obj;
		public Vector3 mcam_vector;
		public Vector3 pvector;
		public int mcam_count;
		public string mapcam_tag = "MapCamera";
		//
		public Quaternion currentcam_quater;//camera transform
		public Vector3 Veccampos;//camera transform
		public Vector3 Vectarget;
		public GameObject target_obj;
		public float damping = 6.0f;
		public bool smooth = true;

		// Use this for initialization
		void Start ()
		{
				//ClosestCamera ();
				target_obj = GameObject.FindGameObjectWithTag ("Player");
				mcam_count = Camera.allCamerasCount;
				camdistances = new float[4];
		}
	
		// Update is called once per frame
		void Update ()
		{

		}

		Camera ClosestCamera ()
		{
				cpos = 10;
				cobj = null;//nothing found just yet.
				mapcam_obj = Camera.allCameras;
				foreach (Camera mcam_obj in mapcam_obj) {
						pvector = target_obj.transform.position;
						mcam_vector = mcam_obj.gameObject.transform.localPosition;
						distance = Vector3.Distance (mcam_vector, pvector);//gets the sqr distance to obj.
						//cpossqr = (mcam_vector - pvector).sqrMagnitude;//gets the sqr distance to obj.

						if (distance < cpos) {//gets the shortest sqr distance.
								mcam_obj.gameObject.tag = "MainCamera";
								cobj = mcam_obj.GetComponent<Camera>();
						} else {
								if (distance > cpos) {//gets the shortest sqr distance.
										mcam_obj.gameObject.tag = "MapCamera";
								} 
						}
				}
				return cobj;//returns the closest obj
		}
		void LateUpdate ()
		{

		}
}