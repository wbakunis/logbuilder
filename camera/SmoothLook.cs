﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("CSharp/Camera-Control/SmoothLook")]

public class SmoothLook : MonoBehaviour
{
		public Quaternion currentcam_quater;//camera transform
		public Vector3 Veccampos;//camera transform
		public Vector3 Vectarget;
		public GameObject target;
		public float damping = 6.0f;
		public bool smooth = true;
		
		void Start ()
		{
				target = GameObject.FindGameObjectWithTag ("Player");
		}

		void LateUpdate ()
		{
				currentcam_quater = transform.rotation;
				Veccampos = transform.position;
				Vectarget = target.transform.position;
				//
				if (smooth == true) {
						// Look at and dampen the rotation
						Quaternion rotation = Quaternion.LookRotation (Vectarget - Veccampos);
						currentcam_quater = Quaternion.Slerp (currentcam_quater, rotation, Time.deltaTime * damping);
				} else {
						// Just lookat
						transform.LookAt (Vectarget);
				}
		}
}