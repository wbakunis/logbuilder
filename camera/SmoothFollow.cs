﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("CSharp/Camera-Control/Smooth Follow")]

public class SmoothFollow : MonoBehaviour
{
		public Transform target;
		public float distance = 10.0f;
		public float height = 5.0f;
		public float heightDamping = 2.0f;
		public float rotationDamping = 3.0f;
		public float wantedRotationAngle;
		public float wantedHeight;
		public float currentRotationAngle;
		public float currentHeight;
		public Vector3 transtarget;
		public Vector3 transcam;
		public Vector3 Quatercam;
		public Vector3 Quatertarget;
    
		void Start ()
		{
				target = GameObject.FindWithTag ("Player").transform;
		}

		void LateUpdate ()
		{
				transtarget = target.position;
				transcam = transform.position;
				Quatercam = transform.eulerAngles;
				Quatertarget = target.eulerAngles;
//
				wantedRotationAngle = Quatertarget.y;
				wantedHeight = transcam.y + height;
				currentRotationAngle = Quatercam.y;
				currentHeight = transcam.y;
//
				currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
				currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
				Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
//
				transcam = transtarget;
				transcam -= currentRotation * Vector3.forward * distance;
				transcam.y = currentHeight;

				transform.LookAt (target);
		}
}