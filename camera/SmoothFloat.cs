﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("CSharp/Camera-Control/SmoothFloat")]

public class SmoothFloat : MonoBehaviour {

    public string[] Controllers;
    public ArrayList ControllerList;
    public bool isBoosting;//is the player using boost speed?

    public Vector3 movement;
    public Vector3 camlocVec;
    public Camera maincamera;
    public Transform Cameratrans;
    public Transform current_target;
    public Transform CameraBase;

    public float vertical;
    public float horizontal;
    public float hover;
    public float current_distance;

    public float xFloatSpeed = 250.0f;
    public float yFloatSpeed = 120.0f;

    public float xRotSpeed = 250.0f;
    public float yRotSpeed = 120.0f;

    private float yRotMinLimit = -20f;
    private float yRotMaxLimit = 80f;

    private float xfloat = 0.0f;
    private float yfloat = 0.0f;
    private float zfloat = 0.0f;
    public float camRotx = 0.0f;
    public float camRoty = 0.0f;
    public float speedMult = 1.1f;
    private float CamBoostMult = 3f;

    private float xlocMin = -100.0f;
    private float xlocMax = 100f;
    private float ylocMin = 0.25f;
    private float ylocMax = 10.0f;
    private float zlocMin = -100.0f;
    private float zlocMax = 100f;

    public bool isControllerEnabled = false;

    void Start() {
        //OnControllerCheck();
        maincamera = Camera.main;
        Cameratrans = maincamera.transform;
        isBoosting = false;//no need to be boosting at the start.
        current_target = GameObject.FindWithTag("StartingPoint").transform;
        CameraBase = GameObject.FindWithTag("CameraBase").transform;
        Vector3 angles = Cameratrans.eulerAngles;
        Vector3 FloatVec = Cameratrans.position;
        xfloat = FloatVec.y;
        yfloat = FloatVec.x;
        zfloat = FloatVec.z;
        camRotx = angles.y;
        camRoty = angles.x;
    }

    void Update() {
        //Debug.Log(isControllerEnabled);
        //OnControllerCheck();
        OnKeyBoardUse();
        //OnUseController();
        CamBoost();
    }

    //public void OnControllerCheck() {
    //    Controllers = Input.GetJoystickNames();
    //    if (Controllers != null) {
    //        isControllerEnabled = true;
    //    } else {
    //        if (Controllers == null) {
    //            isControllerEnabled = false;
    //        }
    //    }
    //}

    //public void OnUseController() {
    //    if (isControllerEnabled) {

    //    }
    //}

    public void OnKeyBoardUse() {
        if (!isControllerEnabled) {
            horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * speedMult;
            vertical = Input.GetAxis("Vertical") * Time.deltaTime * speedMult;
            hover = Input.GetAxis("Hover") * Time.deltaTime * speedMult;

            movement = new Vector3(horizontal, hover, vertical);
            CameraBase.Translate(movement);
            camlocVec = new Vector3(CameraBase.position.x, CameraBase.position.y, CameraBase.position.z);

            camlocVec.x = Mathf.Clamp(camlocVec.x, xlocMin, xlocMax);
            camlocVec.y = Mathf.Clamp(camlocVec.y, ylocMin, ylocMax);
            camlocVec.z = Mathf.Clamp(camlocVec.z, zlocMin, zlocMax);

            CameraBase.position = new Vector3(camlocVec.x, camlocVec.y, camlocVec.z);
        }
    }

    void CamBoost() {
        if ((isBoosting == false) && (Input.GetButton("Boost"))) {
            isBoosting = true;
            speedMult = speedMult + CamBoostMult;
        } else {
            if ((isBoosting == true) && (Input.GetButtonUp("Boost"))) {
                isBoosting = false;
                speedMult = speedMult - CamBoostMult;
            }
        }
    }

    void LateUpdate() {
        if (current_target == null) {
            //Debug.LogError("current_target is null");
        } else {
            camRotx += Input.GetAxis("Mouse X") * xRotSpeed * 0.02f;
            camRoty -= Input.GetAxis("Mouse Y") * yRotSpeed * 0.02f;
            camRoty = ClampAngle(camRoty, yRotMinLimit, yRotMaxLimit);
            Quaternion CamQuatRot = Quaternion.Euler(camRoty, camRotx, 0f);
            Quaternion CamBaseRot = Quaternion.Euler(0f, camRotx, 0f);
            Cameratrans.rotation = CamQuatRot;
            CameraBase.rotation = CamBaseRot;
        }
    }

    static float ClampAngle(float angle, float min, float max) {
        if (angle < -360f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;
        return Mathf.Clamp(angle, min, max);
    }
}