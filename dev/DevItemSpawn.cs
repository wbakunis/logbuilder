﻿using UnityEngine;

public class DevItemSpawn : MonoBehaviour {

    public RaycastManager rcm;
    public GameObject rcm_obj;
    public string rcm_tag = "MainCamera";

    public GameObject Obj0;
    public GameObject Obj1;
    public GameObject Obj2;
    public GameObject Obj3;
    public GameObject Obj4;
    public GameObject Obj5;
    public GameObject Obj6;
    public GameObject Obj7;
    public GameObject Obj8;
    public GameObject Obj9;

    public GameObject lastSpawnedObj;//the last obj to be spawned.
    public Transform lastSpawnedObj_trans;
    public string lastSpawnedObj_name;

    private bool isKey0;
    private bool isKey1;
    private bool isKey2;
    private bool isKey3;
    private bool isKey4;
    private bool isKey5;
    private bool isKey6;
    private bool isKey7;
    private bool isKey8;
    private bool isKey9;

    public bool isDevSpawn;

    public AudioClip Buttonpress_clip;
    public AudioSource devspawn_source;

    // Use this for initialization
    private void Start() {
        devspawn_source = GameObject.FindGameObjectWithTag(rcm_tag).GetComponent<AudioSource>();
        rcm_obj = GameObject.FindGameObjectWithTag(rcm_tag);
        rcm = rcm_obj.GetComponent<RaycastManager>();
    }

    // Update is called once per frame
    private void Update() {
        OnDevSpawnItem();

    }

    public void OnDevSpawnItem() {
        if (Input.GetKeyDown(KeyCode.Alpha0)) {
            devspawn_source.PlayOneShot(Buttonpress_clip);
            OnDevSpawn0();
        } else if (Input.GetKeyDown(KeyCode.Alpha1)) {
            devspawn_source.PlayOneShot(Buttonpress_clip);
            OnDevSpawn1();
            //} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj2, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj3, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha4)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj4, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha5)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj5, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha6)) {
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj6, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha7)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj7, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha8)) {
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj8, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
            //} else if (Input.GetKeyDown(KeyCode.Alpha9)) {
            //    devspawn_source.PlayOneShot(Buttonpress_clip);
            //    if (!rcm.isHoldingBlock) {//is not holding block
            //        Instantiate(Obj9, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
            //    } else {
            //        if (rcm.isHoldingBlock) {//is holding block
            //            Destroy(rcm.HeldLog.gameObject);
            //        }
            //    }
        }
    }

    public void OnDevSpawn0() {
        Instantiate(Obj0, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj0.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj0.gameObject;
        //lastSpawnedObj_name = Obj0.name;
        //lastSpawnedObj_trans = Obj0.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn1() {
        Instantiate(Obj1, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj1.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj1.gameObject;
        //lastSpawnedObj_name = Obj1.name;
        //lastSpawnedObj_trans = Obj1.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn2() {
        Instantiate(Obj2, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj2.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj2.gameObject;
        //lastSpawnedObj_name = Obj2.name;
        //lastSpawnedObj_trans = Obj2.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn3() {
        Instantiate(Obj3, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj3.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj3.gameObject;
        //lastSpawnedObj_name = Obj3.name;
        //lastSpawnedObj_trans = Obj3.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn4() {
        Instantiate(Obj4, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj4.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj4.gameObject;
        //lastSpawnedObj_name = Obj4.name;
        //lastSpawnedObj_trans = Obj4.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn5() {
        Instantiate(Obj5, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj5.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj5.gameObject;
        //lastSpawnedObj_name = Obj5.name;
        //lastSpawnedObj_trans = Obj5.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn6() {
        Instantiate(Obj6, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj6.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj6.gameObject;
        //lastSpawnedObj_name = Obj6.name;
        //lastSpawnedObj_trans = Obj6.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn7() {
        Instantiate(Obj7, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj7.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj7.gameObject;
        //lastSpawnedObj_name = Obj7.name;
        //lastSpawnedObj_trans = Obj7.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn8() {
        Instantiate(Obj8, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj8.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj8.gameObject;
        //lastSpawnedObj_name = Obj8.name;
        //lastSpawnedObj_trans = Obj8.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }
    public void OnDevSpawn9() {
        Instantiate(Obj9, rcm.logholder.position, Quaternion.Euler(0, 0, 0));
        //Obj9.GetComponent<Rigidbody>().isKinematic = true;

        //lastSpawnedObj = Obj9.gameObject;
        //lastSpawnedObj_name = Obj9.name;
        //lastSpawnedObj_trans = Obj9.transform;
        //rcm.HeldLog = lastSpawnedObj;
        //rcm.isHoldingBlock = true;
    }

    //public void OnLogDrop() {
    //    lastSpawnedObj.GetComponent<Rigidbody>().isKinematic = false;
    //    rcm.isHoldingBlock = false;
    //    rcm.HeldLog = null;
    //}

    //public void OnSpawnedObjManipulation() {
    //    if (rcm.isHoldingBlock) {
    //        lastSpawnedObj_trans.position = Vector3.Lerp(lastSpawnedObj_trans.position, rcm.logholder.position, 1);
    //        lastSpawnedObj.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    //        lastSpawnedObj.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
    //    }
    //}
}