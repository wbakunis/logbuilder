﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LogStoreManager : MonoBehaviour {

    //public bool isManufactureWoodButton;
    //public bool isHardwoodStoreButton;
    //public bool isSoftwoodStoreButton;

    public bool isHumanStoreButton;
    public bool isCreatureStoreButton;
    public bool isAnimalStoreButton;

    public bool isWorldStoreButton;
    public bool isSceneryStoreButton;
    public bool isMiscStoreButton;

    /// <summary>
    /// 
    /// </summary>
    public bool isLogStoreFrontButton;
    public bool isEnvironmentStoreFrontButton;
    public bool isEntityStoreFrontButton;
    /// <summary>
    /// 
    /// </summary>
    public bool isLogPieceScroll;
    public bool isEnvironmentObjectsScroll;
    public bool isHumansCreaturesScroll;
    //
    private string LogPiecesScroll_tag = "LogPieceTypeScroll";
    private string LogPiecesButton_tag = "LogPiecesStoreButton";
    private string LogTypesPanel_tag = "LogTypesPanel";
    //private string ManufacturedStore_tag = "ManufacturedStore";
    //private string SoftwoodStore_tag = "SoftwoodStore";
    //private string HardwoodStore_tag = "HardwoodStore";
    //private string SoftwoodPanel_tag = "SoftwoodList";
    //private string HardwoodPanel_tag = "HardwoodList";
    //
    private string EnvironmentObjectsButton_tag = "EnvironmentalObjectStoreButton";
    private string EnvironmentObjectsScroll_tag = "EnvironmentalObjectScroll";
    private string EnvironmentalTypePanel_tag = "EnvironmentalTypePanel";
    private string EnvironmentalScenery_tag = "SceneryStore";
    private string EnvironmentalWorld_tag = "WorldStore";
    private string EnvironmentalMisc_tag = "MiscStore";
    //
    private string EntityButton_tag = "EntityStoreButton";
    private string EntityScroll_tag = "EntityScroll";
    private string EntityTypePanel_tag = "EntityTypePanel";
    private string HumanStore_tag = "HumanStore";
    private string CreatureStore_tag = "CreatureStore";
    private string AnimalStore_tag = "AnimalStore";
    //
    private GameObject EnvironmentTypePanel_obj;
    private GameObject WorldStore_obj;
    private GameObject MiscStore_obj;
    private GameObject SceneryStore_obj;
    //
    private GameObject EntityTypePanel_obj;
    private GameObject HumanStore_obj;
    private GameObject AnimalStore_obj;
    private GameObject CreatureStore_obj;
    //
    //private GameObject HardwoodPanel_obj;
    //private GameObject SoftwoodPanel_obj;
    private GameObject LogTypesPanel_obj;
    //private GameObject ManufacturedStore_obj;
    //private GameObject SoftwoodStore_obj;
    //private GameObject HardwoodStore_obj;
    //
    private GameObject LogPiecesButton_obj;
    private GameObject EnvironmentObjectsButton_obj;
    private GameObject EntityButton_obj;
    private GameObject LogPiecesScroll_obj;
    private GameObject EnvironmentObjectsScroll_obj;
    private GameObject EntityScroll_obj;
    //
    private Vector3 StoreUiOpen_trans;
    private Vector3 StoreUiClosed_trans;
    public GameObject StoreUiOpen_pos;
    public GameObject StoreUiClosed_pos;
    private string StoreUiOpen_tag = "StoreUiOpen_pos";
    private string StoreUiClosed_tag = "StoreUiClosed_pos";
    public GameObject StoreControl_button;
    public GameObject StoreButton_Image;
    public GameObject StorePanel;
    private string StorePanel_tag = "StorePanel";
    private string StoreControl_tag = "StoreButton";
    private string StoreButtonImage_tag = "StoreButtonImage";


    public Component RaycastManager_comp;
    public Component UiManager_comp;
    public GameObject Camera_obj;
    public Transform Camera_trans;
    public GameObject Player_obj;
    public Transform Player_trans;
    private string Player_tag = "Player";
    private string Camera_tag = "MainCamera";

    public Sprite OpenArrow_sprite;
    public Sprite ClosedArrow_sprite;
    public bool isStoreOpen;
    public bool isStoreClosed;

    public int StoreFrontID;//the current page that the user is on.
    private int StoreClosingDelay = 10;

    public GameObject[] LogPieces;//these are the actual log pieces that will be "sold" in the store.
    public int LogPiecesLength;

    // Use this for initialization
    void Start() {
        LogPieces = Resources.LoadAll("LogPieces") as GameObject[];
        LogPiecesLength = LogPieces.Length;
        Debug.Log("LogPieces in store " + LogPiecesLength);
        isStoreOpen = false;
        isStoreClosed = true;
        isEnvironmentStoreFrontButton = false;
        isEntityStoreFrontButton = false;
        isLogStoreFrontButton = false;
        //if (isLogStoreFrontButton) {
        //    isSoftwoodStoreButton = false;
        //    isHardwoodStoreButton = false;
        //}
        OnFindGameObjects();
        //if (HardwoodPanel_obj != null) {
        //    Debug.Log("found hardwood list");
        //}
        //if (SoftwoodPanel_obj != null) {
        //    Debug.Log("found Softwood list");
        //}
    }

    void OnFindGameObjects() {
        EnvironmentTypePanel_obj = GameObject.FindGameObjectWithTag(EnvironmentalTypePanel_tag);
        SceneryStore_obj = GameObject.FindGameObjectWithTag(EnvironmentalScenery_tag);
        WorldStore_obj = GameObject.FindGameObjectWithTag(EnvironmentalWorld_tag);
        MiscStore_obj = GameObject.FindGameObjectWithTag(EnvironmentalMisc_tag);
        //
        EntityTypePanel_obj = GameObject.FindGameObjectWithTag(EntityTypePanel_tag);
        HumanStore_obj = GameObject.FindGameObjectWithTag(HumanStore_tag);
        AnimalStore_obj = GameObject.FindGameObjectWithTag(AnimalStore_tag);
        CreatureStore_obj = GameObject.FindGameObjectWithTag(CreatureStore_tag);
        //
        //HardwoodPanel_obj = GameObject.FindGameObjectWithTag(HardwoodPanel_tag);
        //SoftwoodPanel_obj = GameObject.FindGameObjectWithTag(SoftwoodPanel_tag);
        LogTypesPanel_obj = GameObject.FindGameObjectWithTag(LogTypesPanel_tag);
        //ManufacturedStore_obj = GameObject.FindGameObjectWithTag(ManufacturedStore_tag);
        //SoftwoodStore_obj = GameObject.FindGameObjectWithTag(SoftwoodStore_tag);
        //HardwoodStore_obj = GameObject.FindGameObjectWithTag(HardwoodStore_tag);
        //
        EnvironmentObjectsButton_obj = GameObject.FindGameObjectWithTag(EnvironmentObjectsButton_tag);
        EnvironmentObjectsScroll_obj = GameObject.FindGameObjectWithTag(EnvironmentObjectsScroll_tag);
        LogPiecesButton_obj = GameObject.FindGameObjectWithTag(LogPiecesButton_tag);
        LogPiecesScroll_obj = GameObject.FindGameObjectWithTag(LogPiecesScroll_tag);
        EntityButton_obj = GameObject.FindGameObjectWithTag(EntityButton_tag);
        EntityScroll_obj = GameObject.FindGameObjectWithTag(EntityScroll_tag);
        //
        StoreButton_Image = GameObject.FindGameObjectWithTag(StoreButtonImage_tag);
        StoreControl_button = GameObject.FindGameObjectWithTag(StoreControl_tag);
        //
        StorePanel = GameObject.FindGameObjectWithTag(StorePanel_tag);
        StoreUiOpen_pos = GameObject.FindGameObjectWithTag(StoreUiOpen_tag);
        StoreUiClosed_pos = GameObject.FindGameObjectWithTag(StoreUiClosed_tag);
        //
        //
        Player_obj = GameObject.FindGameObjectWithTag(Player_tag);
        Camera_obj = GameObject.FindGameObjectWithTag(Camera_tag);
        RaycastManager_comp = Camera_obj.GetComponent<RaycastManager>();
        UiManager_comp = Camera_obj.GetComponent<UiManager>();
    }

    // Update is called once per frame
    void Update() {
        StoreUiOpen_trans = StoreUiOpen_pos.transform.position;
        StoreUiClosed_trans = StoreUiClosed_pos.transform.position;
        OnStoreEnable();
        OnBrowseStore();
        OnStoreManualControl();
    }

    public void OnStoreControl() {
        isStoreOpen = !isStoreOpen;
        //OnStoreEnable();
    }

    public void OnStoreManualControl() {
        if (Input.GetButtonDown("LogStore")) {
            isStoreOpen = !isStoreOpen;
            //OnStoreEnable();
        }
    }

    public void OnStoreEnable() {
        if (isStoreOpen) {
            isStoreOpen = true;
            isStoreClosed = true;
            //StoreButton_Image.GetComponent<Image>().sprite = OpenArrow_sprite;
            StorePanel.transform.position = Vector3.Lerp(StorePanel.transform.position, StoreUiOpen_trans, 5f * Time.deltaTime);
        } else {
            if (!isStoreOpen) {
                isStoreClosed = true;
                isStoreOpen = false;
                //StoreButton_Image.GetComponent<Image>().sprite = ClosedArrow_sprite;
                StorePanel.transform.position = Vector3.Lerp(StorePanel.transform.position, StoreUiClosed_trans, 5f * Time.deltaTime);
            }
        }
    }

    /// <summary>
    /// Environmental Click Methods. This is for the UI only. This shouldn't be used with any other methods unless wanting to invoke a "false click".
    /// </summary>

    public void OnBrowseEnvironmentalObjects_Click() {
        isEnvironmentStoreFrontButton = !isEnvironmentStoreFrontButton;
        Debug.Log("isEnvironmentStoreFrontButton" + isEnvironmentStoreFrontButton);
    }

    public void OnBrowseWorld_Click() {
        isWorldStoreButton = !isWorldStoreButton;
        Debug.Log("isWorldStoreButton" + isWorldStoreButton);
    }
    public void OnBrowseScenery_Click() {
        isSceneryStoreButton = !isSceneryStoreButton;
        Debug.Log("isSceneryStoreButton" + isSceneryStoreButton);
    }
    public void OnBrowseMisc_Click() {
        isMiscStoreButton = !isMiscStoreButton;
        Debug.Log("isMiscStoreButton" + isMiscStoreButton);
    }

    /// <summary>
    /// Entity Click Methods. This is for the UI only. This shouldn't be used with any other methods unless wanting to invoke a "false click".
    /// </summary>

    public void OnBrowseEntities_Click() {
        isEntityStoreFrontButton = !isEntityStoreFrontButton;
        Debug.Log("isEntityStoreFrontButton" + isEntityStoreFrontButton);
    }

    public void OnBrowseHuman_Click() {
        isHumanStoreButton = !isHumanStoreButton;
        Debug.Log("isHumanStoreButton" + isHumanStoreButton);
    }
    public void OnBrowseCreature_Click() {
        isCreatureStoreButton = !isCreatureStoreButton;
        Debug.Log("isCreatureStoreButton" + isCreatureStoreButton);
    }
    public void OnBrowseAnimal_Click() {
        isAnimalStoreButton = !isAnimalStoreButton;
        Debug.Log("isAnimalStoreButton" + isAnimalStoreButton);
    }

    /// <summary>
    /// Log Pieces Click Methods. This is for the UI only. This shouldn't be used with any other methods unless wanting to invoke a "false click".
    /// </summary>

    public void OnBrowseLogPieces_Click() {
        isLogStoreFrontButton = !isLogStoreFrontButton;
        Debug.Log("isLogStoreFrontButton" + isLogStoreFrontButton);
    }

    //public void OnBrowseManufactured_Click() {
    //    isManufactureWoodButton = !isManufactureWoodButton;
    //    Debug.Log("isManufactureWoodButton" + isManufactureWoodButton);
    //}

    //public void OnBrowseSoftwood_Click() {
    //    isSoftwoodStoreButton = !isSoftwoodStoreButton;
    //    Debug.Log("isSoftwoodStoreButton " + isSoftwoodStoreButton);
    //}
    //public void OnBrowseHardwood_Click() {
    //    isHardwoodStoreButton = !isHardwoodStoreButton;
    //    Debug.Log("isHardwoodStoreButton " + isHardwoodStoreButton);
    //}

    /// <summary>
    /// the actual store method
    /// </summary>
    /// 

    public int StoreID() {
        return StoreFrontID;
    }

    public void OnBrowseStore() {
        if (isStoreOpen) {
            OnStoreOpen();
            if (isEntityStoreFrontButton) {
                OnBrowseEntityTypes();
            }
            if (isEnvironmentStoreFrontButton) {
                OnBrowseEnvironmentTypes();
            }
            if (isLogStoreFrontButton) {
                OnBrowseLogTypes();

            } else {
                if (!isLogStoreFrontButton) {
                    //reset the two
                    //isSoftwoodStoreButton = false;
                    //isHardwoodStoreButton = false;
                }
            }
        } else {
            if (isStoreClosed) {
                OnStoreClose();
            }
        }
    }

    void OnStoreClose() {
        //StartCoroutine(StoreClosingRoutine());
        isLogStoreFrontButton = false;
        //if (!isLogStoreFrontButton) {
        //    isSoftwoodStoreButton = false;
        //    isHardwoodStoreButton = false;
        //}
        isEntityStoreFrontButton = false;
        isEnvironmentStoreFrontButton = false;
        LogTypesPanel_obj.SetActive(false);
        EntityTypePanel_obj.SetActive(false);
        EnvironmentTypePanel_obj.SetActive(false);
    }

    void OnStoreOpen() {
        isStoreClosed = false;
        StoreFrontID = 0;
        EntityTypePanel_obj.SetActive(isEntityStoreFrontButton);
        EnvironmentTypePanel_obj.SetActive(isEnvironmentStoreFrontButton);
        LogTypesPanel_obj.SetActive(isLogStoreFrontButton);
    }

    void OnBrowseEnvironmentTypes() {
        StoreFrontID = 1;
        isEnvironmentStoreFrontButton = false;
        isLogStoreFrontButton = false;
        EnvironmentTypePanel_obj.SetActive(isEnvironmentStoreFrontButton);
        LogTypesPanel_obj.SetActive(isLogStoreFrontButton);
    }

    void OnBrowseEntityTypes() {
        StoreFrontID = 2;
        isEntityStoreFrontButton = false;
        isLogStoreFrontButton = false;
        EntityTypePanel_obj.SetActive(isEntityStoreFrontButton);
        LogTypesPanel_obj.SetActive(isLogStoreFrontButton);
    }

    void OnBrowseLogTypes() {
        //if (isSoftwoodStoreButton && !isHardwoodStoreButton) {
        //    isHardwoodStoreButton = false;
        //}
        //if (isHardwoodStoreButton && !isSoftwoodStoreButton) {
        //    isSoftwoodStoreButton = false;
        //}
        foreach (GameObject LogPiece in LogPieces) {

        }
        StoreFrontID = 3;
        isEntityStoreFrontButton = false;
        isEnvironmentStoreFrontButton = false;
        EntityTypePanel_obj.SetActive(false);
        EnvironmentTypePanel_obj.SetActive(false);
    }

    IEnumerator StoreClosingRoutine() {
        yield return new WaitForSeconds(StoreClosingDelay);
        isStoreOpen = false;
    }
}