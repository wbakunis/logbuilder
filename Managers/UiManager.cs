﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class UiManager : MonoBehaviour {

    //public LogStoreManager LogStoreManager;

    private string RotationSpeedText_tag = "RotationSpeedText";
    private string StorePanel_tag = "StorePanel";
    private string PauseButton_tag = "PauseButton";
    private string SavingButton_tag = "SavingButton";
    private string LoadingButton_tag = "LoadingButton";
    private string ScreenshotButton_tag = "ScreenshotButton";
    private string SettingsButton_tag = "SettingsButton";
    private string ExitButton_tag = "ExitButton";
    private string PausePanel_tag = "PausePanel";
    private string PlayerUiPanel_tag = "PlayerUiPanel";
    private string CursorLock_tag = "CursorLock";
    public GameObject RotationSpeedText;
    public GameObject StorePanel_obj;
    public GameObject PED_enter_obj;
    public GameObject CursorLock_obj;
    public GameObject PauseButton_obj;
    public GameObject ScreenshotButton_obj;
    public GameObject ExitButton_obj;
    public GameObject SettingsButton_obj;
    public GameObject SavingButton_obj;
    public GameObject LoadingButton_obj;
    public Image CursorLock_image;
    public AudioClip PauseSound;
    public GameObject[] pausescreen;
    public AudioClip pauseclip;
    public AudioClip unpauseclip;
    public AudioClip cameraSnap_clip;

    public SmoothFloat smoothfloat;
    public RaycastManager raycastmanager;
    public Canvas player_Uicanvas;
    public GameObject PlayerUiPanel;

    public bool isGamePaused;//has the player paused the game?
    public bool isOverlayEnabled;// has the player enabled the ui?
    public bool isCursorLocked;
    private bool isSaveButton;
    private bool isLoadButton;
    private bool isPauseButton;
    private bool isSettingsButton;
    private bool isScreenShotButton;

    // Use this for initialization
    void Start() {
        isGamePaused = false;
        isCursorLocked = true;
        OnFindGameObjects();
        foreach (GameObject pauseitems in pausescreen) {
            pauseitems.SetActive(false);
        }
    }

    private void OnFindGameObjects() {
        RotationSpeedText = GameObject.FindGameObjectWithTag(RotationSpeedText_tag);

        SavingButton_obj = GameObject.FindGameObjectWithTag(SavingButton_tag);
        LoadingButton_obj = GameObject.FindGameObjectWithTag(LoadingButton_tag);
        ScreenshotButton_obj = GameObject.FindGameObjectWithTag(ScreenshotButton_tag);
        ExitButton_obj = GameObject.FindGameObjectWithTag(ExitButton_tag);
        PauseButton_obj = GameObject.FindGameObjectWithTag(PauseButton_tag);

        pausescreen = GameObject.FindGameObjectsWithTag(PausePanel_tag);
        CursorLock_obj = GameObject.FindGameObjectWithTag(CursorLock_tag);
        CursorLock_image = CursorLock_obj.GetComponent<Image>();
        CursorLock_image.color = Color.green;
        smoothfloat = Camera.main.GetComponent<SmoothFloat>();
        raycastmanager = Camera.main.GetComponent<RaycastManager>();
        PlayerUiPanel = GameObject.FindGameObjectWithTag(PlayerUiPanel_tag);
        StorePanel_obj = GameObject.FindGameObjectWithTag(StorePanel_tag);
        //LogStoreManager = StorePanel_obj.GetComponent<LogStoreManager>();
    }

    // Update is called once per frame
    void Update() {
        if (isSaveButton) {
            SavingButton_obj.GetComponent<RectTransform>().Rotate(new Vector3(0f, 0f, -90 * Time.deltaTime));
        }
        if (isLoadButton) {
            LoadingButton_obj.GetComponent<RectTransform>().Rotate(new Vector3(0f, 0f, 90 * Time.deltaTime));
        }
        OnUiOverlay();
        OnUiOverlayToggle();
        //UiPauseOverlay();
        OnCursorUnlock();
        OnCursorCheck();
        RotationSpeedText.GetComponent<Text>().text = raycastmanager.BlockRotateSpeedMult.ToString() + "Rot Speed";
        if (Input.GetButtonDown("GamePause")) {
            OnPauseControl();
        }
    }

    public void OnPauseControl() {
        isGamePaused = !isGamePaused;
        OnGamePause();
    }

    public void OnCursorCheck() {
        if (isCursorLocked && !isGamePaused) {//unlock to use menus.
            //Debug.Log("c1");
            smoothfloat.enabled = true;
            raycastmanager.enabled = true;
            CursorLock_image.color = Color.green;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        } else if (isGamePaused && isCursorLocked) {
            //Debug.Log("c2");
            isCursorLocked = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            return;
            //} else if (LogStoreManager.isStoreOpen && isCursorLocked) {
            //    //Debug.Log("c2");
            //    return;
        } else {
            if (!isCursorLocked && !isGamePaused) {
                //Debug.Log("c3_else ending");
                smoothfloat.enabled = false;
                raycastmanager.enabled = false;
                CursorLock_image.color = Color.red;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    public void OnCursorUnlock() {
        if (Input.GetButtonDown("CursorUnlock")) {
            isCursorLocked = !isCursorLocked;
        }
    }

    //public void OnGamePause() {
    //    if (Input.GetButtonDown("GamePause")) {
    //        isGamePaused = !isGamePaused;
    //    }
    //}

    public void OnGamePause() {
        if (isGamePaused) {
            //Debug.Log("1");
            isGamePaused = true;
            isCursorLocked = false;
            Cursor.visible = true;
            //Time.timeScale = 0;
            Camera.main.GetComponent<AudioSource>().PlayOneShot(pauseclip);
            smoothfloat.enabled = false;
            raycastmanager.enabled = false;
            foreach (GameObject pauseitems in pausescreen) {
                pauseitems.SetActive(true);
            }
            //disable the store
            //disable the raymanager
        } else if (!isGamePaused) {
            isCursorLocked = true;
            isGamePaused = false;
            Cursor.visible = false;
            //Debug.Log("2");
            Camera.main.GetComponent<AudioSource>().PlayOneShot(unpauseclip);
            smoothfloat.enabled = true;
            raycastmanager.enabled = true;
            foreach (GameObject pauseitems in pausescreen) {
                pauseitems.SetActive(false);
            }
        } else if (!isGamePaused && !isCursorLocked) {
            //Debug.Log("3");
            isCursorLocked = true;
            isGamePaused = false;
            Camera.main.GetComponent<AudioSource>().PlayOneShot(unpauseclip);
            smoothfloat.enabled = true;
            raycastmanager.enabled = true;
            foreach (GameObject pauseitems in pausescreen) {
                pauseitems.SetActive(false);
            }
        } else if (isGamePaused && !isCursorLocked) {
            //Debug.Log("4");
            isCursorLocked = true;
            isGamePaused = true;
            Camera.main.GetComponent<AudioSource>().PlayOneShot(unpauseclip);
            smoothfloat.enabled = true;
            raycastmanager.enabled = true;
            foreach (GameObject pauseitems in pausescreen) {
                pauseitems.SetActive(false);
            }
        }
    }

    void OnUiOverlayToggle() {
        if (Input.GetButtonDown("UiToggle")) {
            isOverlayEnabled = !isOverlayEnabled;
            if (isOverlayEnabled) {//is true
                Debug.Log(isOverlayEnabled);
            } else {
                Debug.Log(isOverlayEnabled);
            }
        }
    }
    void OnUiOverlay() {
        if (isOverlayEnabled) {
            PlayerUiPanel.SetActive(isOverlayEnabled);
        }
    }
    //
    public void OnSaveButtonHover() {
        if (!isSaveButton) {
            isSaveButton = true;
        }
    }
    public void OnSaveButtonExit() {
        if (isSaveButton) {
            isSaveButton = false;
        }
    }
    //
    public void OnLoadButtonHover() {
        if (!isLoadButton) {
            isLoadButton = true;
        }
    }
    public void OnLoadButtonExit() {
        if (isLoadButton) {
            isLoadButton = false;
        }
    }
    //
    public void OnExitButtonHover() {

    }
    public void OnExitButtonExit() {

    }
    //
    public void OnScreenShotButtonHover() {

    }
    public void OnScreenShotButtonExit() {

    }
    //
    public void OnSettingsButtonHover() {

    }
    public void OnSettingsButtonExit() {

    }

    //    PED_enter_obj = PED_enter.pointerEnter.gameObject;
    //    if (PED_enter_obj == null) {
    //        return;
    //    }
    //    string PED_enter_objtag = PED_enter.pointerEnter.gameObject.tag;
    //    if (PED_enter_objtag == PauseButton_tag) {

    //    } else if (PED_enter_objtag == SavingButton_tag) {
    //        PED_enter_obj.transform.Rotate(Vector3.back);
    //    } else if (PED_enter_objtag == LoadingButton_tag) {
    //        PED_enter_obj.transform.Rotate(Vector3.forward);
    //    } else if (PED_enter_objtag == ScreenshotButton_tag) {
    //        Camera.main.GetComponent<AudioSource>().PlayOneShot(cameraSnap_clip);
    //    } else if (PED_enter_objtag == SettingsButton_tag) {

    //    } else if (PED_enter_objtag == ExitButton_tag) {

    //    }
    //}

    public void OnPointerExit(PointerEventData PED_exit) {

    }
    public void OnPointerClick(PointerEventData PED_click) {

    }
}