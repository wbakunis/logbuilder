﻿using UnityEngine;
using System.Collections;

public class CrosscutManager : MonoBehaviour {

    private RaycastManager rcm;

    public Animation crosscut_anim;
    public AudioSource crosscut_audiosource;
    public AudioClip cutting_clip;
    public Transform crosscut_trans;
    public Sprite HorizontalCut_sprite;
    public Sprite VerticalCut_sprite;
    private GameObject LogtoCut_obj;//the actual log that needs to be cut.


    public float cutspeed;

    private string MainCamera_tag = "MainCamera";
    private string CrossCutSaw_tag = "CrossCutSaw";

    public bool isCutAnimRepeating;//is the crosscut animation repeating?
    public bool isCutAnimPlaying;//is the crosscut animation playing?
    public bool isCrossCutting;
    public bool isVerticalCut;
    public bool isHorizontalCut;

    // Use this for initialization
    void Start() {
        isCrossCutting = false;
        OnFindGameObjects();
    }

    private void OnFindGameObjects() {
        rcm = GameObject.FindGameObjectWithTag(MainCamera_tag).GetComponent<RaycastManager>();
        crosscut_anim = GameObject.FindGameObjectWithTag(CrossCutSaw_tag).GetComponent<Animation>();
        crosscut_audiosource = GameObject.FindGameObjectWithTag(CrossCutSaw_tag).GetComponent<AudioSource>();
        cutting_clip = GameObject.FindGameObjectWithTag(CrossCutSaw_tag).GetComponent<AudioClip>();

    }

    // Update is called once per frame
    void Update() {
        LogtoCut_obj = rcm.HeldLog;
        crosscut_trans = gameObject.transform;
    }



    public void OnCrossCut() {
        if (isVerticalCut) {
            isHorizontalCut = false;
        } else if (isHorizontalCut) {
            isVerticalCut = false;
        }
    }
}
