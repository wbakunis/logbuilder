﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RaycastManager : MonoBehaviour {

    public GameObject SelectedLog_obj;//the log that has been selected but not picked up
    public GameObject HeldLog;//what is the log being held?
    //public Transform HoldLogTransform;
    public Transform logholder;
    public Vector3 loghold_vec;
    private float Xlhvec;
    private float Ylhvec;
    private float Zlhvec;
    private float Xzoom_min = 0f;//min scroll amount
    private float Xzoom_max = 0f;//max amount that the log can be scrolled out
    private float Yzoom_min = 0f;//min scroll amount
    private float Yzoom_max = 0f;//max amount that the log can be scrolled out
    private float Zzoom_min = 0.5f;//min scroll amount
    private float Zzoom_max = 1.25f;//max amount that the log can be scrolled out
    public float lhscroll;//the current scroll
    private float scrollmult = 0.2f;
    private Vector3 logrotvec;
    //public bool isLogKinematic;
    public bool isLogFreeze;
    public bool isResetLog;
    private int Modinc;
    private int CrossSwitch;
    public int BlockRotateSpeedMult;
    private int Modinc_one = 60;
    private int Modinc_two = 65;
    private int Modinc_three = 70;
    private int Modinc_four = 75;
    private int Modinc_five = 80;

    public AudioSource rayAudio;
    public AudioClip wrongclick;
    public AudioClip Scrollclip;

    public Rigidbody logrigidbody;
    public Collider logcollider;

    private Vector3 mousepos;

    public LayerMask LogMask;
    public LayerMask interactablemask;

    private string CrossCutSaw_tag = "CrossCutSaw";
    private string LogHolder_tag = "LogHolder";
    private string PlayerUiCanvas_tag = "PlayerUiCanvas";
    private string logtag = "LogBlock";
    private string InteractableTag = "Interactable";
    private string UntaggedObject = "Untagged";
    private string MouseInfoLabel_tag = "MouseInfoText";

    //public GameObject playerobj;
    //public Transform playertrans;
    //public Vector3 playervector;

    public Canvas PlayerUiCanvas;
    public GameObject mouseinfo_panel;
    public Transform mouseinfo_transform;
    public GameObject MouseInfo_obj;
    public Text mouseinfo_text;
    //public string[] mouseinfo_string;
    public int minfolength;

    private int cursor_height = 1;
    private int cursor_width = 1;
    private int rayMaxDistance = 10;
    public int rayOutOfRange;
    private Cursor mouse_cursor;
    private Texture2D cursor_texture;
    private CursorMode cursor_mode;
    private Vector2 cursor_hotspot;
    private RaycastHit hitpoint;
    public GameObject RayOverObj;
    private LayerMask RayOverObj_layer;
    private string RayOverObj_tag;
    private string RayOverObj_name;

    public Color32 font_color;//currently should be color32. 32bit allows for Alpha transparency
    public int fcR = 0;//red color
    public int fcG = 0;//green color
    public int fcB = 0;//blue color
    public int fcA = 255;//alpha color. use this to make things transparent

    public bool isBlockSelected;//has a block been selected
    public bool isHoldingBlock;
    public bool isMouseInfo_Panel;
    public bool isHoldingAuxillary;

    public LogDetectionManager ldm;
    public CrosscutManager ccm;
    public LineRenderer devline;
    public SpriteRenderer crosshair;

    void Awake() {
    }

    // Use this for initialization
    void Start() {
        //crosshair.material.color = Color.red;
        OnFindGameObjects();
        Modinc = 1;
        OnModerateBlockRotateSpeed(1);
        if (mouseinfo_panel == null) {
            //Debug.Log("not finding the mouse info canvas");
        }
        if (logholder != null) {
            //Debug.Log("Found the log holder!");
        } else {
            //Debug.LogError("Need to find the log holder!");
            logholder = GameObject.FindGameObjectWithTag(LogHolder_tag).transform;
            //Debug.Log("I've found the logholder! Resuming operations.");
        }
        isMouseInfo_Panel = PlayerUiCanvas.enabled;
        //playerobj = GameObject.FindGameObjectWithTag("Player");
        //playertrans = playerobj.transform;
        //playervector = playertrans.position;
        ColorController();
        font_color = new Color(fcR, fcG, fcB, fcA);
        //				CursorInfo ();
        mouseinfo_transform = mouseinfo_panel.transform;
        //mouseinfo_string = new string[minfolength];
        LogMask = LayerMask.GetMask(logtag);
        interactablemask = LayerMask.GetMask(InteractableTag);
    }

    private void OnFindGameObjects() {
        ccm = gameObject.GetComponent<CrosscutManager>();
        crosshair = GameObject.FindGameObjectWithTag(LogHolder_tag).GetComponent<SpriteRenderer>();
        devline = gameObject.GetComponent<LineRenderer>();
        mouseinfo_panel = GameObject.FindGameObjectWithTag(PlayerUiCanvas_tag);
        PlayerUiCanvas = mouseinfo_panel.GetComponent<Canvas>();
        MouseInfo_obj = GameObject.FindGameObjectWithTag(MouseInfoLabel_tag);
        mouseinfo_text = MouseInfo_obj.GetComponent<Text>();
    }

    public void ColorController() {
    }

    public int FontColorR() {
        return fcR;
    }

    public int FontColorG() {
        return fcG;
    }

    public int FontColorB() {
        return fcB;
    }

    public int FontColorA() {
        return fcA;
    }

    //		void CursorInfo ()
    //		{
    //				cursor_height = 1;
    //				cursor_width = 1;
    //				Cursor.SetCursor (cursor_texture, Vector2.zero, cursor_mode);
    //				cursor_texture.height = cursor_height;
    //				cursor_texture.width = cursor_width;
    //				cursor_hotspot = Vector2.zero;
    //				cursor_mode = CursorMode.Auto;
    //		}

    public void OnAuxControl() {
        if (Input.GetButton("Auxillary")) {
            isHoldingAuxillary = true;
        }
        if (Input.GetButtonUp("Auxillary")) {
            isHoldingAuxillary = false;
        }
    }

    void Update() {
        //Debug.Log("Moderator Inc: " + Modinc);
        OnModerateSwitch();
        OnAuxControl();
        //Debug.Log("Aux " + isHoldingAuxillary);
        //if (isHoldingBlock == false) {
        //    HeldLog = null;
        //    logrigidbody = null;
        //}
        rayOutOfRange = rayMaxDistance + 5;
        font_color = new Color(fcR, fcG, fcB, fcA);
        LogHolderManager();
        MouseRayInteraction();
        CurrentlyHolding();
        //LogDrop();
        isMouseInfo_Panel = PlayerUiCanvas.enabled;
    }

    public void LogHolderManager() {
        lhscroll = Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime;
        if (lhscroll < 0) {
            //Debug.Log("[SCROLL]: Less than");
            logholder.Translate(0f, 0f, lhscroll - scrollmult);
            rayAudio.PlayOneShot(Scrollclip, 0.5f);
        } else {
            if (lhscroll > 0) {
                //Debug.Log("[SCROLL]: Greater than");
                logholder.Translate(0f, 0f, lhscroll + scrollmult);
                rayAudio.PlayOneShot(Scrollclip, 0.5f);
            }
        }
        //logholder.localPosition = hitpoint.point;
        loghold_vec = logholder.localPosition;
        //Xlhvec = loghold_vec.x;
        //Ylhvec = loghold_vec.y;
        //Zlhvec = loghold_vec.z;
        loghold_vec.x = Mathf.Clamp(loghold_vec.x, Xzoom_min, Xzoom_max);
        loghold_vec.y = Mathf.Clamp(loghold_vec.y, Yzoom_min, Yzoom_max);
        loghold_vec.z = Mathf.Clamp(loghold_vec.z, Zzoom_min, Zzoom_max);
        logholder.localPosition = loghold_vec;
    }
    public void MouseRayInteraction() {
        mousepos = Input.mousePosition;
        Ray camRay = Camera.main.ScreenPointToRay(mousepos);
        if (Physics.Raycast(camRay, out hitpoint)) {
            if (hitpoint.distance >= rayOutOfRange) {
                Debug.Log("Out of Range. Returning.");
                return;
            }
            devline.SetPosition(1, logholder.localPosition);
            Vector3 camRayVec = camRay.origin;
            float Xcrv = camRayVec.x;
            float Ycrv = camRayVec.y;
            float Zcrv = camRayVec.z;
            MouseRayInfo();
            MouseInfoFromRay();
            string rt = RayOverObj_tag;
            //mouse is hovering over the item. Need to show the item canvas
            if (!isHoldingBlock) {//is not holding a block
                logrigidbody = RayOverObj.GetComponent<Rigidbody>();
                logcollider = RayOverObj.GetComponent<Collider>();
                if (RayOverObj.tag == logtag) {
                    LogOverlayColor();
                    if (Input.GetButtonDown("LeftClick")) {
                        if (!ccm.isCrossCutting) {
                            isHoldingBlock = !isHoldingBlock;
                            HeldLog = logrigidbody.gameObject;
                        } else if (ccm.isCrossCutting) {

                        }

                    } else if (Input.GetButtonDown("RightClick")) {
                        Debug.Log("Crosscut: " + CrossSwitch);
                        OnCrossCutBlock(CrossSwitch += 1);
                        if (CrossSwitch == 4) {
                            OnCrossCutBlock(1);
                            CrossSwitch = 1;
                        }
                    }
                }
                if (RayOverObj.tag == CrossCutSaw_tag) {

                }
                if (RayOverObj.tag != logtag) {
                    UntaggedOverlayColor();
                    if (Input.GetButtonDown("LeftClick")) {
                        //Debug.Log("Untagged object.");
                        rayAudio.volume = 0.15f;
                        rayAudio.clip = wrongclick;
                        rayAudio.Play();
                        //play bloop sound for not grabbing correct item
                    }
                } else {
                    if (RayOverObj.tag != logtag) {
                        UntaggedOverlayColor();
                        if (Input.GetButtonUp("LeftClick")) {
                            rayAudio.volume = 1f;
                        }
                    }
                }
            } else {
                if (isHoldingBlock) {
                    if (Input.GetButtonDown("LeftClick")) {
                        isHoldingBlock = !isHoldingBlock;
                        HeldLog = logrigidbody.gameObject;
                        if (isHoldingBlock == false) {
                            //Debug.Log("MouseRayInteraction(); | Not holding log");
                            logrigidbody.isKinematic = false;
                            isHoldingBlock = false;
                            HeldLog = null;
                        }
                    }
                }
            }
        }
    }

    public void OnBlockHeld() {
        //RayOverObj.GetComponent<Log>
        logrigidbody.isKinematic = true;
        //Debug.Log("is kinematic");
        float Xlrv = logrotvec.x;
        float Ylrv = logrotvec.y;
        float Zlrv = logrotvec.z;
        HeldLog.transform.localPosition = Vector3.Lerp(HeldLog.transform.localPosition, logholder.position, 1);
        logrigidbody.angularVelocity = Vector3.zero;
        logrigidbody.velocity = new Vector3(0f, 0f, 0f);
    }

    public void OnModerateSwitch() {
        //Debug.Log("RotateSpeedMult: " + BlockRotateSpeedMult);
        if (Input.GetButtonDown("SpeedModeratorSwitch")) {
            OnModerateBlockRotateSpeed(Modinc += 1);
        }
        if (Modinc == 6) {
            OnModerateBlockRotateSpeed(1);
            Modinc = 1;
        }
    }

    private void OnCrossCutBlock(int CrossSwitch) {
        switch (CrossSwitch) {
        case 1: {
                //is cutting vertically  
                ccm.isVerticalCut = true;
                ccm.isHorizontalCut = false;
                break;
            }
        case 2: {
                //is cutting horizontally
                ccm.isHorizontalCut = true;
                ccm.isVerticalCut = false;
                break;
            }
        case 3: {
                //is not cutting or doesn't want to cut the log
                ccm.isCrossCutting = false;
                ccm.isHorizontalCut = false;
                ccm.isVerticalCut = false;
                break;
            }
        default: {
                break;
            }
        }
    }

    private void OnModerateBlockRotateSpeed(int Modinc) {
        switch (Modinc) {
        case 1: {
                BlockRotateSpeedMult = Modinc_one;
                //Debug.Log("Mod1");
                break;
            }
        case 2: {
                BlockRotateSpeedMult = Modinc_two;
                //Debug.Log("Mod2");
                break;
            }
        case 3: {
                BlockRotateSpeedMult = Modinc_three;
                //Debug.Log("Mod3");
                break;
            }
        case 4: {
                BlockRotateSpeedMult = Modinc_four;
                //Debug.Log("Mod4");
                break;
            }
        case 5: {
                BlockRotateSpeedMult = Modinc_five;
                //Debug.Log("Mod5");
                break;
            }
        default: {
                //Debug.Log("not sure?");
                Modinc = 1;
                break;
            }
        }
    }

    public int OnReturnSpeedMult() {
        return BlockRotateSpeedMult;
    }

    public void OnHeldBlockRotate() {
        if (!isHoldingAuxillary && Input.GetAxis("RotateLR") < 0) {
            //rotate left and right.
            //Debug.Log("Rotate LR");
            HeldLog.transform.Rotate(new Vector3(0, -1 * Time.deltaTime * BlockRotateSpeedMult, 0), Space.Self);
        } else if (!isHoldingAuxillary && Input.GetAxis("RotateLR") > 0) {
            //rotate left and right.
            //Debug.Log("Rotate LR");
            HeldLog.transform.Rotate(new Vector3(0, 1 * Time.deltaTime * BlockRotateSpeedMult, 0), Space.Self);
        } else if (!isHoldingAuxillary && Input.GetAxis("RotateFB") < 0) {
            //rotate forward and backwards
            //Debug.Log("Rotate FB");
            HeldLog.transform.Rotate(new Vector3(-1 * Time.deltaTime * BlockRotateSpeedMult, 0, 0), Space.Self);
        } else if (!isHoldingAuxillary && Input.GetAxis("RotateFB") > 0) {
            //rotate forward and backwards
            //Debug.Log("Rotate FB");
            HeldLog.transform.Rotate(new Vector3(1 * Time.deltaTime * BlockRotateSpeedMult, 0, 0), Space.Self);
        } else if (isHoldingAuxillary && Input.GetAxis("RotateFB") < 0) {
            //rotate forward and backwards
            //Debug.Log("Rotate FB");
            HeldLog.transform.Rotate(new Vector3(0, 0, -1 * Time.deltaTime * BlockRotateSpeedMult), Space.Self);
        } else if (isHoldingAuxillary && Input.GetAxis("RotateFB") > 0) {
            //rotate forward and backwards
            //Debug.Log("Rotate FB");
            HeldLog.transform.Rotate(new Vector3(0, 0, 1 * Time.deltaTime * BlockRotateSpeedMult), Space.Self);
        }
    }

    public void CurrentlyHolding() {
        if (isHoldingBlock) {
            // HoldLogTransform = HeldLog.transform;
            //Debug.Log(HeldLog.name);
            logrigidbody = HeldLog.GetComponent<Rigidbody>();
            logcollider = HeldLog.GetComponent<Collider>();
            if (logrigidbody) {
                if (HeldLog) {
                    OnBlockHeld();
                    isResetLog = Input.GetButtonDown("ResetLog");
                    if (isResetLog) {
                        isResetLog = true;
                        //Debug.Log("reset log");
                        HeldLog.transform.eulerAngles = Vector3.zero;
                    }
                    if (!isResetLog) {
                        OnHeldBlockRotate();
                    }
                } else {
                    //Debug.LogError("CurrentlyHolding(); | Unable to find held log?");
                    return;
                }
            } else {
                //Debug.LogError("CurrentlyHolding(); | Unable to find log rigidbody?");
                return;
            }
        } else {
            if (!isHoldingBlock) {
                HeldLog = null;
                logrigidbody = null;
            }
        }
    }

    public void MouseRayInfo() {
        RayOverObj = hitpoint.transform.gameObject;
        RayOverObj_layer = RayOverObj.layer;
        RayOverObj_tag = RayOverObj.tag;
        RayOverObj_name = RayOverObj.name;
    }

    public void MouseInfoFromRay() {
        PlayerUiCanvas.enabled = true;
        mouseinfo_transform.position = mousepos;
        mouseinfo_text.text = RayOverObj_tag;
        mouseinfo_text.color = font_color;
    }

    public void LogOverlayColor() {
        // crosshair.material.color = Color.green;
        fcR = 0;
        fcG = 255;
        fcB = 0;
        fcA = 255;
    }
    public void UntaggedOverlayColor() {
        // crosshair.material.color = Color.red;
        fcR = 255;
        fcG = 0;
        fcB = 0;
        fcA = 255;
    }
    public void RayCastIgnoreOverlayColor() {
        //this is for when you're not aiming at anything of interest
        //  crosshair.material.color = Color.green;
        fcR = 255;
        fcG = 75;
        fcB = 0;
        fcA = 255;
    }

    //cleaner option but not used as of 2/22/16
    public void TextColorController() {
        if (RayOverObj.tag == logtag) {
            fcR = 0;
            fcG = 255;
            fcB = 0;
            fcA = 255;
        } else if (RayOverObj.tag == UntaggedObject) {
            fcR = 0;
            fcG = 255;
            fcB = 0;
            fcA = 255;
        } else {
            fcR = 0;
            fcG = 150;
            fcB = 0;
            fcA = 255;
        }
    }
}