﻿using UnityEngine;
using System.Collections;

public class LogDetectionManager : MonoBehaviour {

    //This script is for the logs themselves. This will have methods for when the individual logs are picked up
    //No need to have multiple scripts for each log.

    public AudioSource logAudio;//the log's audio player.

    public Rigidbody logrigidbody;

    //public Transform centernode;

    public bool LogSleep;
    public bool hasBeenSpawned;//has the log just been spawned?
    public bool hasBeenPickedUp;//has the log been picked up?
    public bool hasBeenDestroyed;//has the log been destroyed?

    public float LogCondition;//what is the log's condition?
    public float Full_LogCondition = 100;//what is the log's condition?
    public float Minimal_LogCondition = 0;//what is the log's condition?
    public float Strength;//what is the log's strength?
    public float Weight;//what is the log's weight?

    public float ClunkMagnitude = 1;
    public float ModerateClunkMagnitude = 4;
    public float HeavyClunkMagnitude = 6;
    public float SevereClunkMagnitude = 8;
    public float DestroyClunkMagnitude = 10;//instant destruction

    public float BumpDamage = 1;
    public float ClunkDamage = 3;
    public float ModerateDamage = 5;
    public float HeavyDamage = 7;
    public float SevereDamage = 9;

    public float LogDestroyDelay = 2;

    public AudioClip[] LogAudioClips() {
        AudioClip[] logfx = new AudioClip[] { };
        return logfx;
    }


    // Use this for initialization
    void Start() {
        if (!logrigidbody) {
            logrigidbody = gameObject.GetComponent<Rigidbody>();
        }
        if (logrigidbody.IsSleeping()) {
            logrigidbody.WakeUp();
        }
    }

    // Update is called once per frame
    void Update() {
        LogCondition = Mathf.Clamp(LogCondition, Minimal_LogCondition, Full_LogCondition);
        //clamp log condition between min and full
        //if (LogCondition <= 1) {
        //    //Destroy
        //}
        if (logrigidbody.IsSleeping()) {
            logrigidbody.WakeUp();
        }
    }

    void OnCollisionEnter(Collision logcollision) {
        //Debug.Log("Collision Magnitude:" + logcollision.relativeVelocity.magnitude);
        if (logcollision.relativeVelocity.magnitude <= ClunkMagnitude) {
            LogCondition -= BumpDamage;
            //play sound and particle effect
            //simple clunk
        } else if (logcollision.relativeVelocity.magnitude >= ClunkMagnitude && logcollision.relativeVelocity.magnitude < ModerateClunkMagnitude) {
            LogCondition -= ClunkDamage;
            //play sound and particle effect
            //Loud clunk
        } else if (logcollision.relativeVelocity.magnitude >= ModerateClunkMagnitude && logcollision.relativeVelocity.magnitude <= SevereClunkMagnitude) {
            LogCondition -= ModerateDamage;
            //play sound and particle effect
        } else if (logcollision.relativeVelocity.magnitude >= SevereClunkMagnitude && logcollision.relativeVelocity.magnitude < DestroyClunkMagnitude) {
            LogCondition -= SevereDamage;
            //play sound and particle effect
        } else if (logcollision.relativeVelocity.magnitude >= DestroyClunkMagnitude) {
            //play sound and particle effect
            Destroy(gameObject, LogDestroyDelay);
        } else {
            if (logcollision.relativeVelocity.magnitude < 0) {
                //do nothing
            }
        }
        //for each magnitude > than 3, increase Magnitude.
        //Destroy
    }
}
